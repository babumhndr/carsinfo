var id = localStorage.getItem('id');

if (id == null || id == '' || id == undefined) {
	window.location.href = 'http://localhost/carsinfo/client/login.html';
}


function logout(){
	localStorage.clear();
	window.location.href = 'http://localhost/carsinfo/client/login.html';
}