<!DOCTYPE html>
<html>
<head>
	<title>demo</title>
</head>
<body>
	<div style="height: 100px;width: 200px;background:yellow;position: absolute;" id="content">
		<h1>Bengaluru</h1>
	</div>
	<button id="hideContent">HIDE</button>
	<button id="showContent">SHOW</button>
	<button id="toggleContent">TOGGLE</button>
	<button id="fadeInContent">FADE IN</button>
	<button id="fadeOutContent">FADE OUT</button>
	<button id="fadeToggleContent">FADE TOGGLE</button>
	<button id="fadeToContent">FADE TO</button>
	<button id="slideToggleContent">SLIDE TOGGLE</button>
	<button id="animateContent">ANIMATE</button>
</body>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript">
	$('#hideContent').click(function(){
		$('#content').hide(2000);
	});

	$('#showContent').click(function(){
		$('#content').show(1000);
	});

	$('#toggleContent').click(function(){
		$('#content').toggle(1000);
	});

	$('#fadeInContent').click(function(){
		$('#content').fadeIn(1000);
	});

	$('#fadeOutContent').click(function(){
		$('#content').fadeOut(1000);
	});

	$('#fadeToggleContent').click(function(){
		$('#content').fadeToggle(1000);
	});
	$('#fadeToContent').click(function(){
		$('#content').fadeTo(1000,0.2);
	});

	$('#slideToggleContent').click(function(){
		$('#content').slideToggle(1000);
	});

	$('#animateContent').click(function(){
		$('#content').animate({height: "200px",left:"200px",top:"300px", borderRadius:"50%"});

		$('#content').animate({height: "200px",left:"600px",top:"0px", borderRadius:"50%"});
	});
</script>
</html>