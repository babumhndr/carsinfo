-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2018 at 08:36 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carsinfo`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `password` varchar(256) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `mobile`, `password`, `created`) VALUES
(1, 'Ahalya', 'ahalya@gmail.com', '987456321', '12321', '2018-07-21 23:32:21'),
(2, 'balu', 'ahalya@gmail.cmm', '9797979797', '321412', '2018-07-21 23:34:24'),
(3, 'bob', 'bob@gmail.com', '9846321456', '12321', '2018-07-27 21:57:46'),
(5, 'Aaradhya', 'aaradhya@gmail.com', '98745621441', '12321', '2018-07-27 23:10:57'),
(6, '', '', '', '', '2018-07-27 23:48:49'),
(9, 'Aaradhya', 'aaradhysssa@gmail.com', '98745621441', '12321', '2018-07-27 23:49:43'),
(11, 'bobb', 'bobb2@gmail.comm', '98745632100', '12321', '2018-07-28 21:14:03'),
(14, 'carsinfoa', 'vinithvkk050@gmail.com', '98745632100', '12321', '2018-07-28 21:19:27'),
(15, 'xyz', 'xyz@gmail.com', '123456789', '1231', '2018-07-28 21:21:05'),
(17, 'aradya', 'aradya@gmail.com', '98745632100', '1231', '2018-07-28 22:00:00'),
(19, 'qwert', 'wert@wert.com', '12345654321', '78787', '2018-07-28 22:00:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
